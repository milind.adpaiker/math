# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Copy the local package files to the container's workspace.
ADD . /go/src/math

RUN cd /go/src/math && go build -o math main.go && chmod 755 math

WORKDIR /go/src/math
# Run the outyet command by default when the container starts.
ENTRYPOINT /go/src/math/math

# Document that the service listens on port 8080.
EXPOSE 8080